package org.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem5 {
    public static Map<String, Double> calculateAveragescore(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Integer> totalScorePerInnings = new HashMap<>();
        Map<String, Integer> inningsCount = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String battingTeam = delivery[2];
                        int totalRuns = Integer.parseInt(delivery[17]);
                        int innings = Integer.parseInt(delivery[11]);
                        totalScorePerInnings.put(battingTeam, totalScorePerInnings.getOrDefault(battingTeam, 0) + totalRuns);
                        inningsCount.put(battingTeam, inningsCount.getOrDefault(battingTeam, 0) + innings);
                    }
                }
            }
        }

        Map<String, Double> averageScorePerInnings = new HashMap<>();
        for (String team : totalScorePerInnings.keySet()) {
            int totalScore = totalScorePerInnings.get(team);
            int count = inningsCount.get(team);
            double average = totalScore / (double) count;
            averageScorePerInnings.put(team, average);
        }

        return averageScorePerInnings;
    }
}

