package org.example;

import java.util.*;

public class Problem4 {
    public static Map<String, Double> calculateTopbowlers(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Double> bowlerEconomy = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();
        Map<String, Integer> bowlerRuns = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String bowler = delivery[8];
                        int runs = Integer.parseInt(delivery[17]);
                        int balls = 1;
                        bowlerBalls.put(bowler, bowlerBalls.getOrDefault(bowler, 0) + balls);
                        bowlerRuns.put(bowler, bowlerRuns.getOrDefault(bowler, 0) + runs);
                    }
                }
            }
        }

        for (String bowler : bowlerBalls.keySet()) {
            int balls = bowlerBalls.get(bowler);
            int runs = bowlerRuns.get(bowler);
            double economy = runs / (balls / 6.0);
            bowlerEconomy.put(bowler, economy);
        }
        List<Map.Entry<String, Double>> sortedBowlerEconomy = new ArrayList<>(bowlerEconomy.entrySet());
        sortedBowlerEconomy.sort(Map.Entry.comparingByValue());

        Map<String, Double> topEconomicalBowlers = new LinkedHashMap<>();
        for (int i = 0;i<3&&i < sortedBowlerEconomy.size(); i++) {
            Map.Entry<String, Double> entry = sortedBowlerEconomy.get(i);
            topEconomicalBowlers.put(entry.getKey(), entry.getValue());
        }

        return topEconomicalBowlers;
    }

}
