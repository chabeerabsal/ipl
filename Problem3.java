package org.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem3 {
    public static Map<String, Integer> calculateExtraruns(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Integer> extraRunsConceded = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String bowlingTeam = delivery[3];
                        int extraRuns = Integer.parseInt(delivery[16]);
                        extraRunsConceded.put(bowlingTeam, extraRunsConceded.getOrDefault(bowlingTeam, 0) + extraRuns);
                    }
                }
            }
        }

        return extraRunsConceded;
    }

}
