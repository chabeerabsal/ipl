package org.example;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            String matchcsvfile = "/home/welcome/ipl/matches.csv";
            String delcsvfile = "/home/welcome/ipl/deliveries.csv";

            List<String[]> matchrecord = run(matchcsvfile);
            List<String[]> delrecord = run(delcsvfile);
            Map<Integer, Integer> matchesPerYear = getMatchesPerYear(matchrecord);
            Map<String, Integer> matchesWonByTeams = getMatchesWonByTeams(matchrecord);
          //  Map<String, Integer> extraRunsConceded2016 = getExtraRunsConceded(matchrecord, delrecord, 2016);
            Map<String, Integer> extraruns = calculateExtraruns(matchrecord, delrecord, "2016");
            Map<String, Double> topbowlers = calculateTopbowlers(matchrecord, delrecord, "2015");
            Map<String, Double> averagescore = calculateAveragescore(matchrecord, delrecord, "2015");

            System.out.println("1 Number of matches played per year:");
            System.out.println(Problem1.getMatchesPerYear(matchrecord));
           // System.out.println(matchesPerYear);
            System.out.println("\n2 Number of matches won by teams:");
            System.out.println(Problem2.getMatchesWonByTeams(matchrecord));

            System.out.println("\n3 Extra runs conceded per team in 2016:");
           System.out.println(Problem3.calculateExtraruns(matchrecord,delrecord,"2016"));
            System.out.println("\n4 Top economical bowlers in 2015: \n" + Problem4.calculateTopbowlers(matchrecord,delrecord,"2016"));
            System.out.println("\n5 Average score per innings for each team in 2015:\n" + Problem5.calculateAveragescore(matchrecord,delrecord,"2015"));

        } catch (Exception e) {
            System.out.println("Exception occurs");
        }
    }

    private static Map<Integer, Integer> getMatchesPerYear(List<String[]> matchrecord) {
        Map<Integer, Integer> matchesPerYear = new TreeMap<>();

        for (String[] record : matchrecord) {
           if (isInteger(record[1])) {
                int year = Integer.parseInt(record[1]);
                matchesPerYear.put(year, matchesPerYear.getOrDefault(year, 0) + 1);
            }
        }

        return matchesPerYear;
    }

            private static Map<String, Integer> getMatchesWonByTeams(List<String[]> matchesData) {
        Map<String, Integer> matchesWon = new HashMap<>();

        for (String[] record : matchesData) {

            String wi = record[10];
            if (wi.length() >= 2 && (wi.equals("winner")) != true)
                matchesWon.put(wi, matchesWon.getOrDefault(wi, 0) + 1);
        }

        return matchesWon;
    }

    private static boolean isInteger(String value) {
        try {
            int number=Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


       private static List<String[]> run(String csvfile) throws IOException {
        List<String[]> ll = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(csvfile))) {
            String line="";
            while ((line = bf.readLine()) != null) {
                String[] array = line.split(",");
                ll.add(array);
            }
        }
        return ll;
    }
    private static Map<String, Integer> calculateExtraruns(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Integer> extraRunsConceded = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String bowlingTeam = delivery[3];
                        int extraRuns = Integer.parseInt(delivery[16]);
                        extraRunsConceded.put(bowlingTeam, extraRunsConceded.getOrDefault(bowlingTeam, 0) + extraRuns);
                    }
                }
            }
        }

        return extraRunsConceded;
    }


    private static Map<String, Double> calculateTopbowlers(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Double> bowlerEconomy = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();
        Map<String, Integer> bowlerRuns = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String bowler = delivery[8];
                        int runs = Integer.parseInt(delivery[17]);
                        int balls = 1;
                        bowlerBalls.put(bowler, bowlerBalls.getOrDefault(bowler, 0) + balls);
                        bowlerRuns.put(bowler, bowlerRuns.getOrDefault(bowler, 0) + runs);
                    }
                }
            }
        }

        for (String bowler : bowlerBalls.keySet()) {
            int balls = bowlerBalls.get(bowler);
            int runs = bowlerRuns.get(bowler);
            double economy = runs / (balls / 6.0);
            bowlerEconomy.put(bowler, economy);
        }
        List<Map.Entry<String, Double>> sortedBowlerEconomy = new ArrayList<>(bowlerEconomy.entrySet());
        sortedBowlerEconomy.sort(Map.Entry.comparingByValue());

        Map<String, Double> topEconomicalBowlers = new LinkedHashMap<>();
        for (int i = 0;i<3&&i < sortedBowlerEconomy.size(); i++) {
            Map.Entry<String, Double> entry = sortedBowlerEconomy.get(i);
            topEconomicalBowlers.put(entry.getKey(), entry.getValue());
        }

        return topEconomicalBowlers;
}

    private static Map<String, Double> calculateAveragescore(List<String[]> matches, List<String[]> deliveries, String year) {
        Map<String, Integer> totalScorePerInnings = new HashMap<>();
        Map<String, Integer> inningsCount = new HashMap<>();

        for (String[] match : matches) {
            if (match[1].equals(year)) {
                String matchId = match[0];
                for (String[] delivery : deliveries) {
                    if (delivery[0].equals(matchId)) {
                        String battingTeam = delivery[2];
                        int totalRuns = Integer.parseInt(delivery[17]);
                        int innings = Integer.parseInt(delivery[11]);
                        totalScorePerInnings.put(battingTeam, totalScorePerInnings.getOrDefault(battingTeam, 0) + totalRuns);
                        inningsCount.put(battingTeam, inningsCount.getOrDefault(battingTeam, 0) + innings);
                    }
                }
            }
        }

        Map<String, Double> averageScorePerInnings = new HashMap<>();
        for (String team : totalScorePerInnings.keySet()) {
            int totalScore = totalScorePerInnings.get(team);
            int count = inningsCount.get(team);
            double average = totalScore / (double) count;
            averageScorePerInnings.put(team, average);
        }

        return averageScorePerInnings;
    }
}