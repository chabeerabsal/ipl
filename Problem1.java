package org.example;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Problem1 {
    public static Map<Integer, Integer> getMatchesPerYear(List<String[]> matchrecord) {
        Map<Integer, Integer> matchesPerYear = new TreeMap<>();

        for (String[] record : matchrecord) {
            if (isInteger(record[1])) {
                int year = Integer.parseInt(record[1]);
                matchesPerYear.put(year, matchesPerYear.getOrDefault(year, 0) + 1);
            }
        }

        return matchesPerYear;
    }
    private static boolean isInteger(String value) {
        try {
            int number=Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
