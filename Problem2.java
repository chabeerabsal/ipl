package org.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem2 {
    public static Map<String, Integer> getMatchesWonByTeams(List<String[]> matchesData) {
        Map<String, Integer> matchesWon = new HashMap<>();

        for (String[] record : matchesData) {

            String wi = record[10];
            if (wi.length() >= 2 && (wi.equals("winner")) != true)
                matchesWon.put(wi, matchesWon.getOrDefault(wi, 0) + 1);
        }

        return matchesWon;
    }
}
